#!/bin/bash

FLYCAP=/home/uav/FlyCapReqs
CATKIN=/home/uav/catkin_ws


rm -rf $CATKIN/build/pointgrey_camera_driver/
cp -r $FLYCAP/pointgrey_camera_driver/ $CATKIN/build/
cp -v $FLYCAP/libflycapture.so.2 $CATKIN/devel/lib/
