first setup the nvme drive:

1) run "sudo nvme list" to see if harddrive is attached. Example output:

Node             SN                   Model                                    Namespace Usage                      Format           FW Rev  
---------------- -------------------- ---------------------------------------- --------- -------------------------- ---------------- --------
/dev/nvme0n1     S3X4NB0K405246R      Samsung SSD 960 EVO 500GB                1          40.33  GB / 500.11  GB    512   B +  0 B   3B7QCXE7





2) find the UUID by running show_nvme_uuid.sh or entering: "sudo blkid | grep nvme | grep "\bUUID" ".
Example output:

/dev/nvme0n1p1: UUID="1f5f75e5-978a-44b2-ae96-e8b54e337a94" TYPE="ext4" PARTLABEL="Linux filesystem" PARTUUID="e136228b-e9a8-42a2-b9e0-e9982518cee9"

Now you have the UUID!





3) Now edit the fstab and swap out the UUID with your UUID you found.

Example fstab: 
# <file system> <mount point>             <type>          <options>                               <dump> <pass>
/dev/root            /                    rootfs          defaults                                     0 1
/dev/disk/by-uuid/1f5f75e5-978a-44b2-ae96-e8b54e337a94 /mnt/nvme_drive auto nosuid,nodev,nofail,x-gvfs-show 0 0

swap the UUID after /by-uuid/ with your own UUID.
then sudo reboot.

You should be able to see some contents on the hard drive now under /mnt/nvme_drive
