# FlyCap2_libs_ros_setup

flir shared libs for flycap because the website keeps changing and ros node login credentials are no longer valid


1) add uav to video and audio user groups
   - check groups of uav to see if it has video & audio there. (run "groups uav") 
   - if not add with: sudo usermod -a -G audio,video uav
   - reboot
2) setup nvme drive
   - follow instructions in setup_nvme_after_flash.md
   - reboot
   - sudo chmod 777 /mnt/nvme_drive
   - make file /mnt/nvme_drive/out.mkv
   - sudo chmod 777 /mnt/nvme_drive/out.mkv
3) setup catkin_ws
   - mkdir -p ~/catkin_ws/src
   - cd ~/catkin_ws
   - catkin_make
4) clone repos in src & make, pointgrey_camera_driver will break missing Flycap2
   - cd ~/catkin_ws/src
   - git clone https://github.com/kmartin36/ros_deep_learning
   - git clone https://gitlab.com/pwolfeGTRI/ros_image_to_rtsp
   - git clone https://github.com/FinnLinxxx/pointgrey_camera_driver
   - cd ..
   - catkin_make
5) setup Flycap2 lib:
   - move FlyCapReqs directory to /home/uav
   - run ~/FlyCapReqs/CopyFlycap2Catkin_ws.sh
   - cd ~/catkin_ws
   - catkin_make

Use the flea3_custom_args.launch (you can put it in ~/catkin_ws/src/pointgrey_camera_driver/pointgrey_camera_driver/launch)

Should build and work now. test using HOWTO-run-camera-demo file in this repo. don't forget to source:
/opt/ros/kinetic/setup.bash
~/catkin_ws/devel/setup.bash 
